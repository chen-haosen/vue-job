## 使用props传递参数

使用props参数，可以更方便的给路由组件传递数据。

本篇基于路由的query参数中的完整实例，实现需求：从路由组件Message.vue向路由组件Detail.vue传递数据。

路由规则配置中，props参数的使用有如下三种方式：

props值是一个对象。
props值是一个布尔值。
props值是一个函数。

看下这3种方式。

props值是一个对象

在Detail的路由规则中配置props值为一个对象，该对象中的key-value会全部以props的形式传递给Detail组件。

修改router/index.js

对应的路由添加上props

props:{
        id:"xxx",
        title:"yyy"
     }

对应的组件暴露props
<template>
    <ul>
        <li>消息编号：{{id}}</li>
        <li>消息标题：{{title}}</li>
    </ul>
</template>

<script>
export default {
    name:"Detail",
    props:['id','title']
}
</script>

<style>

</style>


props值是一个布尔值

对应的路由

props:true

对应的组件

props:['id','title']

对应的props 是个函数

props($route){
             return {
                      id:$route.params.id,
                      title:$route.params.title
                     }
            }

对应的组件
props:['id','title']


