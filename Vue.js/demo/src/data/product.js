export default {
    name:'product',
    products:[
            {id:1,name:'韩国三熹玉3CE彩妆香水专场',img:'../assets/3ce.jpg',num:80,price:1000},
            {id:2,name:'阿玛尼彩妆专场',img:'../assets/阿玛尼.jpg',num:90,price:279},
            {id:3,name:'芙丽芳丝洗面奶专场',img:'../assets/芙丽芳丝洗面奶.jpg',num:80,price:3000},
            {id:4,name:'橘朵口红彩妆',img:'../assets/橘朵口红.jpg',num:67,price:3000},
            {id:5,name:'科颜氏',img:'../assets/科颜氏.jpg',num:67,price:3000},
            {id:6,name:'兰蔻',img:'../assets/兰蔻.jpg',num:67,price:3000},
            {id:7,name:'神仙水',img:'../assets/神仙水.jpg',num:67,price:3000},
            {id:8,name:'雅诗兰黛眼霜',img:'../assets/雅诗兰黛眼霜.jpg',num:67,price:3000},
            {id:6,name:'滋色粉饼',img:'../assets/滋色粉饼.jpg',num:67,price:3000},
            {id:7,name:'olay小白瓶',img:'../assets/olay小白瓶.jpg',num:67,price:3000},
        ],
    cart:[],
    addToCart:function(id){
        let inCart = false;
        this.cart.forEach(el=>{
            if(id==el.id){
                inCart=true;
                el.num++;
            }
        })
        if(inCart==false){
            this.products.forEach(el=>{
              if(el.id==id){
                  let product =el;
                  product.num=1;
                  this.cart.push(product);
              }
            })          
        }
        console.log(this.cart)
    },
    getCart(){
        return this.cart;
    },   
}