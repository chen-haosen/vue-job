import Vue from 'vue'
import App from './App.vue'
// import axios from 'axios'

//引入VueRouter
import VueRouter from 'vue-router'
//应用插件
Vue.use(VueRouter)

// Vue.use(axios)


//引入路由器
import router from './router/index'


//关闭生产环境提交
Vue.config.productionTip = false
//render 渲染 template
new Vue({
  el:'#app',
  // template:'<App></App>',
  // components:{
  //   App,
  // }
  //脚手架引入的vue是残缺的，为了性能
  render: (createElement) => {
    // console.log(typeof createElement);
    return createElement(App)
   },
   router
})
