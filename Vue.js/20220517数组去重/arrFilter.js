let arr=[1,2,2,3,4,6,9,10,9,8,20];
 newArr=[]
// 第一种暴力双循环重复法
// for(let i=0;i<arr.length;i++){
//     for(let j=i+1;j<arr.length;j++){
//         if(arr[i]==arr[j]){
//             arr.slice(j,i);
//             j--;
//         }
//     }
// }
// console.log(newArr);

// includes去重法
for(let i of arr){
    if(!newArr.includes(i)){
        newArr.push(i);
    }else{
    console.log(i+'重复了');
    }
}

// 第三种方法filter()循环过滤法
// arr.filter((val,index)=>{
//     if(newArr.indexOf(val)<0){
//         newArr.push(val)
//     }
// })

// sort()排序法(不常用？)
// arr=arr.sort();
// for(let i=0;i<arr.length;i++){
//     if(i<arr.length-1&&arr[i]!=arr[i+1]){
//         newArr.push(arr[i]);
//     }
// }

// Es6方法
// console.log(Array.from(new Set(arr)));
// console.log([...new Set(arr)]);//简写方式
// console.log(newArr);

// 终极版  递归思想
// let index=0;
// arr=arr.sort();
// function loop(index){
//     if(index>1){
//         if(arr[index]==arr[index-1]){
//             arr.splice(index,1)
//         }
//         index--;
//         loop(index)
//     }
// }
// loop(arr.length-1);
// console.log(arr);